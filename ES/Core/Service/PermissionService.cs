﻿using Core.Interfaces;
using Domain.Entities;
using Domain.Interfaces.Base;
using Domain.Interfaces.Repositories;
using Presistence.Context;

namespace Core.Service
{
    public class PermissionService : IPermissionService
    {
        protected IUnitOfWork<ConceptContext> _unitOfWork { set; get; }
        private readonly IPermissionRepository _permissionRepository;
        private readonly IElasticSearchRespository<Permission> _elasticSearchRespository;

        public PermissionService(IUnitOfWork<ConceptContext> unitOfWork, IPermissionRepository permissionRepository, IElasticSearchRespository<Permission> elasticSearchRespository)
        {
            this._unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this._permissionRepository = permissionRepository ?? throw new ArgumentNullException(nameof(permissionRepository));
            this._elasticSearchRespository = elasticSearchRespository ?? throw new ArgumentNullException(nameof(elasticSearchRespository));
        }

        public async Task<List<Permission>> GetAllPermissions()
        {
            try
            {
                var getAll = await _permissionRepository.GetAllPermissions();
                return getAll;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<Permission> ModifyPermission(Permission model)
        {
            try
            {
                var addPermissionLocal = _permissionRepository.ModifyPermission(model);
                var index = await this.IndexElastic(model);

                return await addPermissionLocal;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private async Task<Permission> IndexElastic(Permission model)
        {
            var index = await _elasticSearchRespository.Index(model);
            return index;
        }

        public async Task<Permission> GetPermissionById(int permissionId)
        {
            try
            {
                var getSingle = await _permissionRepository.GetPermissionById(permissionId);
                return getSingle;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
