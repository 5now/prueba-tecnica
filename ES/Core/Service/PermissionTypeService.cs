﻿using Core.Interfaces;
using Domain.Entities;
using Domain.Interfaces.Base;
using Domain.Interfaces.Repositories;
using Presistence.Context;

namespace Core.Service
{
    public class PermissionTypeService : IPermissionTypeService
    {
        protected IUnitOfWork<ConceptContext> _unitOfWork { set; get; }
        private readonly IPermissionTypeRepository _permissionTypeRepository;

        public PermissionTypeService(IUnitOfWork<ConceptContext> unitOfWork, IPermissionTypeRepository permissionTypeRepository)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this._permissionTypeRepository = permissionTypeRepository ?? throw new ArgumentNullException(nameof(permissionTypeRepository));
        }

        public async Task<List<PermissionType>> GetPermissionType()
        {
            try
            {
                var getAll = await _permissionTypeRepository.GetPermissionType();
                return getAll;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
