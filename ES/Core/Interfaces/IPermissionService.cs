﻿using Domain.Entities;

namespace Core.Interfaces
{
    public interface IPermissionService
    {
        Task<List<Permission>> GetAllPermissions();
        Task<Permission> ModifyPermission(Permission model);
        Task<Permission> GetPermissionById(int permissionId);
    }
}
