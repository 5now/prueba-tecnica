﻿namespace Core.Interfaces
{
    public interface IPermissionTypeService
    {
        Task<List<Domain.Entities.PermissionType>> GetPermissionType();
    }
}
