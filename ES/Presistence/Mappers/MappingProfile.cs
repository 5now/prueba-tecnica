﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presistence.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Messages
            CreateMap<Presistence.DataModels.Permission, Domain.Entities.Permission>().ReverseMap();
            CreateMap<Presistence.DataModels.PermissionType, Domain.Entities.PermissionType>().ReverseMap();
            #endregion
        }
    }
}
