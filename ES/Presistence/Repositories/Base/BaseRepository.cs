﻿using AutoMapper;
using Domain.Interfaces.Base;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Presistence.Repositories.Base
{
    public class BaseRepository<TContext, T> : IBaseRepository<T> where T : class, new() where TContext : DbContext
    {
        #region Private Fields

        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        protected TContext Context { get; set; }

        /// <summary>
        /// The mapper
        /// </summary>
        protected IMapper _mapper { set; get; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository{TContext, T}"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public BaseRepository(TContext context) => Context = context;

        #endregion

        public void Add(T entity)
        {
            var entitySet = Context.Set<T>();
            entitySet.Add(entity);
            Context.SaveChanges();
        }

        public async Task<T> AddAsync(T entity)
        {
            var entitySet = Context.Set<T>();
            await entitySet.AddAsync(entity);
            await Context.SaveChangesAsync();
            return entity;
        }

        public void AddList(List<T> entities)
        {
            var entitySet = Context.Set<T>();
            entitySet.AddRange(entities);
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return GetAll().Any(predicate);
        }

        public void Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteList(List<T> entities)
        {
            throw new NotImplementedException();
        }

        public void DeleteWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public IQueryable<T> GetAll()
        {
            var entitySet = Context.Set<T>();
            return entitySet.AsQueryable();
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return GetAll().Where(predicate);
        }

        public async Task<IQueryable<T>> GetAllAsync()
        {
            var entitySet = await Context.Set<T>().ToListAsync();
            return entitySet.AsQueryable();
        }

        public async Task<IQueryable<T>> GetAllAsync(Expression<Func<T, bool>> predicate)
        {
            var entitySet = await Context.Set<T>().Where(predicate).ToListAsync();
            return entitySet.AsQueryable();
        }

        public T GetSingle(Expression<Func<T, bool>> predicate)
        {
            return GetAll().AsNoTracking().FirstOrDefault(predicate);
        }

        public async Task<T> GetSingleAsync(Expression<Func<T, bool>> predicate)
        {
            return await Context.Set<T>().SingleOrDefaultAsync(predicate);
        }

        public void Update(T entity)
        {
            var entitySet = Context.Set<T>();
            entitySet.Update(entity);
            Context.SaveChanges();
        }

        public async Task<T> UpdateAsync(T entity)
        {
            var entitySet = Context.Set<T>();
            entitySet.Update(entity);
            await Context.SaveChangesAsync();
            return entity;
        }

        public List<T> GetByParam(Expression<Func<T, bool>> predicate)
        {
            return Context.Set<T>().Where(predicate).ToList();
        }

        public async Task<IQueryable<T>> GetByParamAsync(Expression<Func<T, bool>> predicate)
        {
            var entitySet = await Context.Set<T>().Where(predicate).ToListAsync();
            return entitySet.AsQueryable();
        }
    }
}
