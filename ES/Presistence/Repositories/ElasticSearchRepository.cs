﻿using Domain.Interfaces.Repositories;
using Nest;

namespace Presistence.Repositories
{
    public class ElasticSearchRepository<TDocument> : IElasticSearchRespository<TDocument> where TDocument : class
    {
        private readonly IElasticClient _elasticClient;

        public ElasticSearchRepository(IElasticClient elasticClient)
        {
            this._elasticClient = elasticClient ?? throw new ArgumentException(nameof(elasticClient));
        }

        public async Task<TDocument> Index(TDocument model)
        {
            var response = await _elasticClient.IndexAsync<TDocument>(model, x => x.Index("permissions"));
            return model;
        }

        public async Task<List<TDocument>> Index(List<TDocument> model)
        {
            if (model != null && model.Count > 0)
            {
                foreach (var item in model)
                {
                    var response = await _elasticClient.IndexAsync<TDocument>(item, x => x.Index("permissions"));
                }
            }

            return model;
        }
    }
}
