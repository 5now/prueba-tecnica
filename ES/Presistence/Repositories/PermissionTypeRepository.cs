﻿using AutoMapper;
using Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Presistence.DataModels;
using Presistence.Repositories.Base;

namespace Presistence.Repositories
{
    public class PermissionTypeRepository<TContext> : BaseRepository<TContext, PermissionType>, IPermissionTypeRepository where TContext : DbContext
    {
        protected TContext _Context { get; set; }
        public PermissionTypeRepository(TContext context, IMapper mapper) : base(context)
        {
            _Context = context;
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

        }

        public async Task<List<Domain.Entities.PermissionType>> GetPermissionType()
        {
            var getAll = await base.GetAllAsync();
            return _mapper.Map<List<Domain.Entities.PermissionType>>(getAll);
        }
    }
}
