﻿using AutoMapper;
using Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Presistence.DataModels;
using Presistence.Repositories.Base;

namespace Presistence.Repositories
{
    public class PermissionRepository<TContext> : BaseRepository<TContext, Permission>, IPermissionRepository where TContext : DbContext
    {
        protected TContext _Context { get; set; }
        public PermissionRepository(TContext context, IMapper mapper) : base(context)
        {
            _Context = context;
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

        }

        public async Task<List<Domain.Entities.Permission>> GetAllPermissions()
        {
            var getAll = await base.GetAllAsync();
            return _mapper.Map<List<Domain.Entities.Permission>>(getAll);
        }

        public async Task<Domain.Entities.Permission> GetPermissionById(int permissionId)
        {
            var getSingle = await base.GetSingleAsync(c => c.Id == permissionId);
            return _mapper.Map<Domain.Entities.Permission>(getSingle);
        }

        public async Task<Domain.Entities.Permission> ModifyPermission(Domain.Entities.Permission model)
        {
            var mapp = _mapper.Map<Permission>(model);

            var permissionExist = base.GetSingle(x => x.Id == model.Id);
            if (permissionExist == null)
                await base.AddAsync(mapp);
            await base.UpdateAsync(mapp);
            model.Id = mapp.Id;
            return model;
        }
    }
}
