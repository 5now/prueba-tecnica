﻿using Microsoft.EntityFrameworkCore;
using Presistence.DataModels;

namespace Presistence.Context
{
    public class ConceptContext : DbContext
    {
        public ConceptContext(DbContextOptions<ConceptContext> options) : base(options)
        {
        }

        public virtual DbSet<Permission> Permission { get; set; }
        public virtual DbSet<PermissionType> PermissionType { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Permission>().HasKey(x => x.Id);
            modelBuilder.Entity<PermissionType>().HasKey(x => x.Id);
        }
    }
}
