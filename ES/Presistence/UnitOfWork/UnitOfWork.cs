﻿using AutoMapper;
using Domain.Interfaces.Base;
using Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Presistence.Repositories;

namespace Presistence.UnitOfWork
{
    public class UnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DbContext
    {
        private readonly TContext _context;
        protected IMapper _mapper { set; get; }

        public UnitOfWork(TContext context, IMapper mapper)
        {
            this._context = context ?? throw new ArgumentNullException(nameof(context));
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void BeginTransaction()
        {
            _context.Database.BeginTransaction();

            this.InTransaction = true;
        }

        public void BeginTransactionAsync()
        {
            _context.Database.BeginTransactionAsync();

            this.InTransaction = true;
        }

        public async Task<IDbContextTransaction> TaskBeginTransactionAsync()
        {
            var trans = await _context.Database.BeginTransactionAsync();

            this.InTransaction = true;
            return trans;
        }

        public void Commit()
        {
            _context.Database.CommitTransaction();

            this.InTransaction = false;
        }

        public void CommitAsync()
        {
            _context.Database.CommitTransactionAsync();

            this.InTransaction = false;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void Rollback()
        {
            if (this.InTransaction)
                _context.Database.RollbackTransaction();
        }

        public void RollbackAsync()
        {
            if (this.InTransaction)
                _context.Database.RollbackTransactionAsync();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public bool InTransaction { get; set; }

        public IPermissionRepository PermissionRepository
        {
            get { return new PermissionRepository<TContext>(this._context, this._mapper); }
        }
    }
}
