﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Presistence.DataModels
{
    [Table("Permissions")]
    public class Permission
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string EmployeeFirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string EmployeeLastName { get; set; }

        [Required]
        public int PermissionTypeId { get; set; }

        [Required]
        public DateTime PermissionDate { get; set; } = DateTime.Now;

        [ForeignKey("PermissionTypeId")]
        public PermissionType PermissionType { get; set; }
    }
}
