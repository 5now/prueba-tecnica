﻿using Core.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Api.Query.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PermissionTypeController : ControllerBase
    {
        private readonly IPermissionTypeService _permissionTypeService;

        public PermissionTypeController(IPermissionTypeService permissionTypeService)
        {
            this._permissionTypeService = permissionTypeService ?? throw new ArgumentException(nameof(permissionTypeService));
        }

        [Route("GetPermissionType")]
        [HttpGet]
        [SwaggerOperation(Summary = "", Description = "", OperationId = "GetPermissionType")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<PermissionType>), description: "successful operation")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 500, type: typeof(string), description: "Internal Server Error")]
        public async Task<IActionResult> GetPermissionType()
        {
            var response = await this._permissionTypeService.GetPermissionType();
            return base.Ok(response);
        }
    }
}
