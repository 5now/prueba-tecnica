﻿using Core.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Api.Query.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PermissionController : ControllerBase
    {
        private readonly IPermissionService _permissionService;

        public PermissionController(IPermissionService permissionService)
        {
            this._permissionService = permissionService ?? throw new ArgumentException(nameof(permissionService));
        }

        [Route("GetAllPermissions")]
        [HttpGet]
        [SwaggerOperation(Summary = "", Description = "", OperationId = "GetAllPermissions")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Permission>), description: "successful operation")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 500, type: typeof(string), description: "Internal Server Error")]
        public async Task<IActionResult> GetAllPermissions()
        {
            var response = await this._permissionService.GetAllPermissions();
            return base.Ok(response);
        }

        [Route("RequestPermissions")]
        [HttpGet]
        [SwaggerOperation(Summary = "", Description = "", OperationId = "RequestPermissions")]
        [SwaggerResponse(statusCode: 200, type: typeof(Permission), description: "successful operation")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 500, type: typeof(string), description: "Internal Server Error")]
        public async Task<IActionResult> RequestPermissions()
        {
            var response = await this._permissionService.GetAllPermissions();
            return base.Ok(response);
        }

        [Route("GetPermissionById")]
        [HttpGet]
        [SwaggerOperation(Summary = "", Description = "", OperationId = "GetPermissionById")]
        [SwaggerResponse(statusCode: 200, type: typeof(Permission), description: "successful operation")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 500, type: typeof(string), description: "Internal Server Error")]
        public async Task<IActionResult> GetPermissionById(int permissionId)
        {
            var response = await this._permissionService.GetPermissionById(permissionId);
            return base.Ok(response);
        }
    }
}
