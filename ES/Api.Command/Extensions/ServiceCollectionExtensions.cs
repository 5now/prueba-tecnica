﻿using AutoMapper;
using Core.Interfaces;
using Core.Service;
using Domain.Entities;
using Domain.Interfaces.Base;
using Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Presistence.Context;
using Presistence.Mappers;
using Presistence.Repositories;
using Presistence.UnitOfWork;

namespace Api.Command.Extensions
{
    internal static class ServiceCollectionExtensions
    {
        internal static void AddServicesConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ConceptContext>(c => c.UseSqlServer(configuration.GetConnectionString("ConceptoElastic")));

            // AutoMapper
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            // Repositories and Unit of Work
            InitializeRepositoriesAndUnitOfWork(services, configuration);

            // Services
            InitializeServices(services);

            // Database
            InitializeDatabaseConnection(services, configuration);
        }

        private static void InitializeRepositoriesAndUnitOfWork(IServiceCollection services, IConfiguration configuration)
        {
            #region Local Repository

            services.AddScoped<IElasticSearchRespository<Permission>, ElasticSearchRepository<Permission>>();
            services.AddScoped<IPermissionRepository, PermissionRepository<ConceptContext>>();
            services.AddScoped<IUnitOfWork<ConceptContext>, UnitOfWork<ConceptContext>>();

            #endregion
        }

        private static void InitializeServices(IServiceCollection services)
        {
            #region Application Service
            //services.AddScoped<OperatorApplicationService>();
            #endregion


            #region Services
            services.AddScoped<IPermissionService, PermissionService>();
            #endregion
        }

        private static void InitializeDatabaseConnection(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ConceptContext>((serviceProvider) =>
            {
                var options = new DbContextOptionsBuilder<ConceptContext>()
                    .UseSqlServer(configuration.GetConnectionString("ConceptoElastic"))
                    .Options;

                var context = new ConceptContext(options);
                return context;
            });
        }
    }
}
