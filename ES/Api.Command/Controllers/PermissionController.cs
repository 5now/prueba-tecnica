﻿using Core.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Nest;
using Swashbuckle.AspNetCore.Annotations;

namespace Api.Command.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PermissionController : ControllerBase
    {
        private readonly IPermissionService _permissionService;
        private readonly IElasticClient _elasticClient;

        public PermissionController(IPermissionService permissionService, IElasticClient elasticClient)
        {
            this._permissionService = permissionService ?? throw new ArgumentException(nameof(permissionService));
            this._elasticClient = elasticClient ?? throw new ArgumentException(nameof(elasticClient));
        }

        [Route("ModifyPermission")]
        [HttpPost]
        [SwaggerOperation(Summary = "", Description = "", OperationId = "ModifyPermission")]
        [SwaggerResponse(statusCode: 200, type: typeof(Permission), description: "successful operation")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 500, type: typeof(string), description: "Internal Server Error")]
        public async Task<IActionResult> ModifyPermission(Permission model)
        {
            var response = await _permissionService.ModifyPermission(model);
            return base.Ok(response);
        }
    }
}
