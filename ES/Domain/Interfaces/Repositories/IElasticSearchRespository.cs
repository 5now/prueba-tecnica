﻿namespace Domain.Interfaces.Repositories
{
    public interface IElasticSearchRespository<T>
    {
        Task<T> Index(T model);
        Task<List<T>> Index(List<T> model);
    }
}
