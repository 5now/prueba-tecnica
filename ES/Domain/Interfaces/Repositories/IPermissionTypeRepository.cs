﻿namespace Domain.Interfaces.Repositories
{
    public interface IPermissionTypeRepository
    {
        Task<List<Domain.Entities.PermissionType>> GetPermissionType();
    }
}
