﻿using Domain.Entities;

namespace Domain.Interfaces.Repositories
{
    public interface IPermissionRepository
    {
        Task<List<Permission>> GetAllPermissions();
        Task<Domain.Entities.Permission> ModifyPermission(Permission model);
        Task<Domain.Entities.Permission> GetPermissionById(int permissionId);
    }
}
