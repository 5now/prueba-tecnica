﻿namespace Domain.Interfaces.Base
{
    public interface IBaseUnitOfWork
    {
        #region Public Sync Operations 

        /// <summary>
        /// Begins the transaction.
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <returns></returns>
        int SaveChanges();

        /// <summary>
        /// Commits the transaction.
        /// </summary>
        void Commit();

        /// <summary>
        /// Rollbacks the transaction.
        /// </summary>
        void Rollback();

        #endregion

        #region Public Async Operations 

        /// <summary>
        /// Begins the transaction asynchronous.
        /// </summary>
        void BeginTransactionAsync();

        /// <summary>
        /// Gets the save changes asynchronous.
        /// </summary>
        /// <value>
        /// The save changes asynchronous.
        /// </value>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// Commits the asynchronous.
        /// </summary>
        void CommitAsync();

        /// <summary>
        /// Rollbacks the asynchronous.
        /// </summary>
        void RollbackAsync();

        #endregion
    }
}
