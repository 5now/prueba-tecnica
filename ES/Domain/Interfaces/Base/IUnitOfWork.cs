﻿using Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Domain.Interfaces.Base
{
    public interface IUnitOfWork<TContext> : IBaseUnitOfWork, IDisposable where TContext : DbContext
    {
        IPermissionRepository PermissionRepository { get; }
    }
}
