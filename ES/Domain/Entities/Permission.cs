﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Permission
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string EmployeeFirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string EmployeeLastName { get; set; }

        [Required]
        public int PermissionTypeId { get; set; }

        [Required]
        public DateTime PermissionDate { get; set; } = DateTime.Now;
    }
}
