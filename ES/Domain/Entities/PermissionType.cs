﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class PermissionType
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }
    }
}
