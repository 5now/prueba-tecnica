import React, { Component } from "react";
import { PermissionForm } from "../../Component/Permission/PermissionForm";
import PermissionTable from "../../Component/Permission/PermissionTable";
import Collapse from "@mui/material/Collapse";
import Alert from "@mui/material/Alert";
import IconButton from "@mui/material/IconButton";

export class PermissionIndex extends Component {
  constructor(props) {
    super(props);
    this.state = {
      permissionId: 0,
      openSuccess: false
    };
  }

  render() {
    return (
      <div>
        <Collapse in={this.state.openSuccess}>
          <Alert
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  this.setState({ openSuccess: false });
                }}
              >
                x
              </IconButton>
            }
            sx={{ mb: 2 }}
          >
            Permiso guardado con éxito
          </Alert>
        </Collapse>

        {this.state.permissionId == 0 ? (
          <PermissionTable
            isEdit={(a) => {
              this.setState({ permissionId: a });
            }}
          ></PermissionTable>
        ) : (
          <PermissionForm
            isEdit={(a) => {
              this.setState({ permissionId: a });
            }}
            openSuccess = {(a)=>{
                this.setState({ openSuccess: a });
            }}
            PermissionId={this.state.permissionId}
          ></PermissionForm>
        )}
      </div>
    );
  }
}
