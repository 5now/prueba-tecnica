import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import {PermissionIndex} from "./Pages/Permission/Index"

import Layout from "./Component/Layout";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout></Layout>}>
            <Route path="permisos" element={<PermissionIndex></PermissionIndex>} />
          </Route>
        </Routes>
      </BrowserRouter>      
    </div>
  );
}

export default App;
