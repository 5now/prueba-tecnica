import * as React from "react";
import Box from "@mui/material/Box";
import axios from "axios";
import { DataGrid } from "@mui/x-data-grid";
import { Button } from "@mui/material";
import {URL_BASE_QUERY} from "../../Providers/Api"

export default function PermissionTable(props) {
  const handleChange = (event) => {
    if (props.isEdit) {
      props.isEdit(event);
    }
  };

  const columns = [
    { field: "id", headerName: "ID", width: 90 },
    {
      field: "employeeFirstName",
      headerName: "Nombre",
      width: 300,
      editable: true,
    },
    {
      field: "employeeLastName",
      headerName: "Apellido",
      width: 300,
      editable: true,
    },
    {
      field: "permissionTypeId",
      headerName: "Id del permiso",
      type: "number",
      width: 110,
      editable: true,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      width: 300,
      cellClassName: "actions",
      getActions: ({ id }) => {
        return [
          <Button
          
            icon={":)"}
            label="Edit"
            className="textPrimary"
            onClick={(e) => {
              handleChange(id);
            }}
            color="inherit"
          >
            Editar
          </Button>,
        ];
      },
    },
  ];

  const [permissionsData = [], setPermissionType] = React.useState(null);

  React.useEffect(() => {
    axios
      .get(`${URL_BASE_QUERY}api/Permission/GetAllPermissions`)
      .then((response) => {
        setPermissionType(response.data);
      });
  }, []);

  return (
    <Box sx={{ p: 10 }}>
      <Button onClick={()=>{handleChange(null)}}>Crear nuevo</Button>
      {permissionsData != null ? (
        <Box sx={{ height: 400, width: "100%" }}>
          <DataGrid
            rows={permissionsData}
            columns={columns}
            pageSize={2}
            rowsPerPageOptions={[2]}
            checkboxSelection
            disableSelectionOnClick
            experimentalFeatures={{ newEditingApi: true }}
          />
        </Box>
      ) : (
        <span>Sin información</span>
      )}
    </Box>
  );
}
