import * as React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import { Button } from "@mui/material";
import axios from "axios";
import {URL_BASE_QUERY} from "../../Providers/Api"

export default function ComboPermissionsType(props) {
  const handleChange = (event) => {
    setNewValue(event.target.value);
    if (props.callBack) {
      props.callBack(event);
    }
  };

  const [permissionsData = [], setPermissionType] = React.useState(null);
  const [newValue, setNewValue] = React.useState(null);

  React.useEffect(() => {
    axios
      .get(`${URL_BASE_QUERY}api/PermissionType/GetPermissionType`)
      .then((response) => {
        setPermissionType(response.data);        
      });
  }, []);

  return (
    <TextField
      required
      id="filled-select-currency"
      select
      label="Tipo de permiso"
      helperText="Seleccione el tipo de permiso"
      variant="filled"
      onChange={handleChange}
      value={newValue == null ? props.PermissionId: newValue}
      focused={props.PermissionId != null}
    >
      {permissionsData != null
        ? permissionsData.map((option) => (
            <MenuItem key={option.id} value={option.id} >
              {option.description}
            </MenuItem>
          ))
        : null}
    </TextField>
  );
}
