import React, { Component } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import { Button } from "@mui/material";
import ComboPermissionsType from "./ComboPermissionType";
import axios from "axios";
import {URL_BASE_QUERY, URL_BASE_COMMAND} from "../../Providers/Api"


export class PermissionForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openSuccess: false,
      model: {
        EmployeeFirstName: null,
        EmployeeLastName: null,
        PermissionTypeId: null,
      },
      permissionId: props.PermissionId,
      dataPermission: {},
    };
  }

  componentDidMount() {
    this.GetPermissionById();
  }

  async GetPermissionById() {
    axios
      .get(
        `${URL_BASE_QUERY}api/Permission/GetPermissionById?permissionId=${this.state.permissionId}`
      )
      .then((response) => {
        this.setState({
          model: {
            Id: response.data.id,
            EmployeeFirstName: response.data.employeeFirstName,
            EmployeeLastName: response.data.employeeLastName,
            PermissionTypeId: response.data.permissionTypeId,
          },
        });
      });
  }

  setModel(property, value) {
    const newModel = Object.assign({}, this.state.model, {
      [property]: value,
    });

    this.setState({
      model: newModel,
    });
  }

  async savePermission(event, context) {
    event.preventDefault();

    axios
      .post(
        `${URL_BASE_COMMAND}api/Permission/ModifyPermission`,
        this.state.model
      )
      .then(function (response) {
        context.setState({ openSuccess: true });
        context.props.isEdit(0);
        context.props.openSuccess(true);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
      <Box
        component="form"
        autoComplete="off"
        sx={{
          "& .MuiTextField-root": { m: 1, width: "50ch" },
        }}
        onSubmit={(e) => this.savePermission(e, this)}
      >
        <div>
          <TextField
            id="outlined-select-currency"
            label="Nombre"
            helperText="Ingrese el nombre del empleado"
            required
            onChange={(a) => {
              this.setModel("EmployeeFirstName", a.currentTarget.value);
            }}
            value={this.state.model.EmployeeFirstName}
            focused={this.state.model.EmployeeLastName != null}
          ></TextField>
          <TextField
            id="outlined-select-currency"
            label="Apellido"
            helperText="Ingrese el apellido del empleado"
            required
            onChange={(a) => {
              this.setModel("EmployeeLastName", a.currentTarget.value);
            }}
            value={this.state.model.EmployeeLastName}
            focused={this.state.model.EmployeeLastName != null}
          ></TextField>
        </div>
        <div>
          <ComboPermissionsType
            callBack={(a) => {
              this.setModel("PermissionTypeId", a.target.value);
            }}
            PermissionId={this.state.model.PermissionTypeId}
          ></ComboPermissionsType>
        </div>
        <div>
          <Button type="button" onClick={() => this.props.isEdit(0)} variant="outlined">
            Cancelar
          </Button>
          <Button type="submit" variant="contained">Enviar</Button>
        </div>
      </Box>
    );
  }
}
